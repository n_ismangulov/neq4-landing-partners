<?php
/**
 * Отправить сообщение на почту info@neq4.ru
 */
require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

$titles = [
    'name' => "ФИО",
    'email' => "Имейл",
    'phone' => "Телефон",
    'company' => "Компания",
];

parse_str(file_get_contents("php://input"), $data);

if (empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'
) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;

$mail->isSMTP();
$mail->Host = 'smtp.yandex.ru';
$mail->SMTPAuth = true;
$mail->Username = 'mailer@neq4.ru';
$mail->Password = 'ZovK3NCrZE';
$mail->SMTPSecure = 'tls';
$mail->Port = 587;

$mail->setFrom('mailer@neq4.ru', 'neq4.ru/partners');
$mail->addAddress('info@neq4.ru', 'neq4');
$mail->isHTML(false);

$mail->CharSet = 'UTF-8';
$mail->Subject = 'Партнерская программа - регистрация';

$mail->Body = "";
foreach ($titles as $key => $value) {
    if (empty($data[$key])) continue;

    $mail->Body .= "{$value}: {$data[$key]}" . PHP_EOL;
}

$result = $mail->send();

die(json_encode([
    'result' => $result ? 'success' : 'failed'
]));
